import * as THREE from 'https://cdn.jsdelivr.net/npm/three@0.125.2/build/three.module.js';
import { DeviceOrientationControls } from 'https://cdn.jsdelivr.net/npm/three@0.125.2/examples/jsm/controls/DeviceOrientationControls.js';
/* Global vars */
let camera, scene, renderer;
let controls;
let eggs = [];
let areEggsReady = false;
let collected = [];
let maxEggsToCollect = 20;
const map = new THREE.TextureLoader().load( './IMG/eggCollected.png' );
map.wrapS = THREE.RepeatWrapping;
map.repeat.set( 2, 1 );
const raycaster = new THREE.Raycaster();
const mouse = new THREE.Vector2();
const music = new Audio("./MUSIC/neighborhood-pals_by_rex-banner_Artlist.mp3");
music.loop = true;
const startButton = document.getElementById( 'startButton' );
startButton.addEventListener( 'click', function () {

    document.getElementById("overlay").style.display = "none";
    music.play();
    init();
    animate();


} );

function init() {

    const container = document.getElementById('container');
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1100);
    scene = new THREE.Scene();
    const geometry = new THREE.SphereGeometry(10, 32, 32);
    geometry.scale(-1, 1, 1);

    const video = document.getElementById('video');
    video.play();
    //video.pause();
    const texture = new THREE.VideoTexture(video);
    const material = new THREE.MeshBasicMaterial({ map: texture });
    camera.lookAt(-1.8,-4,-7)
    const mesh = new THREE.Mesh(geometry, material);
    scene.add(mesh);
    const light = new THREE.AmbientLight( 0xffffff ); // 224c80
    scene.add( light );
    renderer = new THREE.WebGLRenderer();
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);
    controls = new DeviceOrientationControls( camera );
    controls.alphaOffset = 0.03;
    placeAllEggs(scene);
    /* Event listeners */
    window.addEventListener('click', function(e){
        onMouseClick(e);
    })
    window.addEventListener('resize', onWindowResize);
}

function generateEasterEgg(position, eggRadius){
    const eggGeometry = new THREE.SphereGeometry(eggRadius,32,32);
    const eggMaterial = new THREE.MeshPhongMaterial({ map: map, opacity: 0, transparent: true });
    const eggMesh = new THREE.Mesh(eggGeometry, eggMaterial);
    eggMesh.position.set(position.x, position.y, position.z);
    eggMesh.name = "egg";
    return eggMesh;
}
function placeAllEggs(scene) {
    eggs.push(generateEasterEgg(new THREE.Vector3(0.38, -2.5, -7), 0.4));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(-6.8, -3.4, -5), 0.5));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(6.3, -2.7, 1.7), 0.6));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(-5, -1.9, 0), 0.4));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(-8, -1.175, -4.2), 0.3));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(4.9, -1.1, -1.8), 0.25));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(3.22, -0.55, -4.2), 0.2));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(3.45, -2, 9), 0.5));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(0.4, -0.4, 9), 0.55));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(7.8, -1.7, 5), 0.32));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(-3, -1.25, 5.5), 0.3));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(-4, -0.3, 2.5), 0.2));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(-2.55, 1.1, -1.3), 0.08));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(-1.3, 1.2, -2), 0.14));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(-1.75, -0.28, 3), 0.2));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(-0.5, -0.2, -3), 0.1));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(-1.13, -0.6, -3), 0.13));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(0.12, -0.6, -3), 0.13));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(-3.1, -0.7, 2), 0.2));
    scene.add(eggs[eggs.length - 1]);
    eggs.push(generateEasterEgg(new THREE.Vector3(-6, -1, 0.45), 0.2));
    scene.add(eggs[eggs.length - 1]);
    areEggsReady = true;
    document.getElementById("score").getElementsByTagName("p")[0].innerHTML = collected.length + "/" + maxEggsToCollect;
}
function collectEgg(egg){
    collected.push(egg);
    egg.material.transparent = false;
    eggs.splice(eggs.indexOf(egg), 1);
    document.getElementById("score").getElementsByTagName('p')[0].innerHTML = collected.length + "/" + maxEggsToCollect;
    if(collected.length >= maxEggsToCollect){
        document.getElementById("popUp").style.visibility = "visible";
    }
    
}
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

function onMouseClick(event) {
    // calculate mouse position in normalized device coordinates
	// (-1 to +1) for both components
	mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
    
    raycaster.setFromCamera( mouse, camera );
	// calculate objects intersecting the picking ray
	const intersects = raycaster.intersectObjects( eggs);
	for ( let i = 0; i < intersects.length; i ++ ) {

		collectEgg(intersects[0].object);

	}

}
function animate() {
    window.requestAnimationFrame( animate );
    controls.update();
    if (areEggsReady){
        eggs.forEach(egg => {
            egg.lookAt(0,0,0);
        });
    }
    update();
}

function update() {
    renderer.render(scene, camera);
}